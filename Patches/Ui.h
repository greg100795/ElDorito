#pragma once

namespace Patches
{
	namespace Ui
	{
		void EnableCenteredCrosshairPatch(bool enable);

		void ApplyAll();
		void ApplyMapNameFixes();
	}
}